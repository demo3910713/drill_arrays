
function flatten(nestedArray) {
    let result = [];

    for (let index = 0; index < nestedArray.length; index++) {
        const element = nestedArray[index];
        if (Array.isArray(element)) {
            result = result.concat(flatten(element));
        } else {
            result.push(element);
        }
    }

    return result;
}
module.exports = { flatten };
