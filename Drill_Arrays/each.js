function each(items, cb) {
  // check the data correct or not
  if (Array.isArray(items)) {
    for (let index = 0; index < items.length; index++) {
      cb(items[index], index);
    }
  } else {
    ("Data Insufficent");
  }
}

// callback Function
function cb(value, index) {
  console.log(`Element at index ${value} is ${index}`);
}
// Export the code
module.exports = { each, cb };
