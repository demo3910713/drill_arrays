function reduce(elements, cb, startingValue) {
  // check the given data correct or not
  if (Array.isArray(elements)) {
    let total = startingValue;
    for (let index = 0; index < elements.length; index++) {
      const element = elements[index];
      total = cb(total, element);
    }
    return total;
  } else {
    return " data Insufficent ";
  }
}

// call back function
function cb(startingValue, value) {
  let operation = startingValue + value;
  return operation;
}

module.exports = { reduce, cb };
