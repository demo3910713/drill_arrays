function find(items, cb) {
  if (Array.isArray(items)) {
    let res = false;
    for (let index = 0; index < items.length; index++) {
      const element = items[index];
      if (res == false) {
        res = cb(element, res);
      } else {
        break;
      }
    }
    return res;
  } else {
  }
}
// call back function
function cb(value, array) {
  if (value % 2 == 0) {
    return true;
  } else {
    return false;
  }
}

module.exports = { find, cb };
